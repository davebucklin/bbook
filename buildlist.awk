#Copyright (C) 2023 Dave Bucklin <dave@19a6.net>
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
BEGIN{
  data_dir="$HOME/.bbook"
  contacts_path=data_dir "/contacts.rec"
  cmd = "recsel " opts " \"" contacts_path "\""
  while ((cmd | getline) > 0) {
    if ($0 != ""){
      pos = index($0,":")
      key = substr($0,1,pos-1)
      value = substr($0,pos+2)
      print value
    }
  }
  close(cmd)
  # Date Time Content
}
  
